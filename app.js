var express = require("express");
var app = express();
var request = require("request");
var bodyParser = require("body-parser");


app.use(bodyParser.urlencoded({extended: true}));

app.get("/results", function(req, res) {
    var movie = req.query.movieName;
    var reqMovie = "http://www.omdbapi.com/?s=" + movie + "&apikey=thewdb";
    request(reqMovie, function(error, response, body) {
        if (!error & response.statusCode == 200) {
            var data = JSON.parse(body);
            res.render("results.ejs", {data:data});
            
        }    
    })
})

app.get("/", function(req, res) {
    res.render("form.ejs");
})


app.listen(3000, () => console.log("server is listening!"));
